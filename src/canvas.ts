export class Canvas {

    public static width: number = 640;
    public static height: number = 400;
    public static context: CanvasRenderingContext2D;

    public static init(el: HTMLCanvasElement) {
        el.width = Canvas.width;
        el.height = Canvas.height;
        Canvas.context = el.getContext("2d");
    }

    public static fill_rect(x: number, y: number, w: number, h: number, color: string) {
        Canvas.context.beginPath();
        Canvas.context.fillStyle = color;
        Canvas.context.fillRect(x, y, w, h);
    }

    public static fill_bkg() {
        var gradient = Canvas.context.createRadialGradient(Canvas.width, Canvas.height, Canvas.width * 0.1, Canvas.width / 2, Canvas.height / 2, Canvas.width);
        Canvas.context.beginPath();
        gradient.addColorStop(0, 'blue');
        gradient.addColorStop(1, 'Black');
        Canvas.context.fillStyle = gradient;
        Canvas.context.fillRect(0, 0, Canvas.width, Canvas.height);
    }

    public static clear() {
        Canvas.context.clearRect(0, 0, this.width, this.height);
    }

}
