import { Canvas } from './canvas.js'

enum FoodType {
    Normal = 10, //Default amount of points, infinite lifetime
    Rare = 50, //Extra points, 10 seconds of life
    Boost = 25, //Default points, gives speed boost
    Relocate = 25, //Default points, sets snake position to random
    Mushroom = 100, //Default points, 5 seconds lifetime
}

export class Food {
    x: number;
    y: number;
    width: number = 10;
    color: string = 'Yellow';
    type: FoodType = FoodType.Normal;

    constructor() {
        let x = Math.floor((Math.random() * Canvas.width) + 1);
        let y = Math.floor((Math.random() * Canvas.height) + 1);

        this.x = this.fix_number(x, 'x');
        this.y = this.fix_number(y, 'y');
    }

    respawn() {
        this.x = this.fix_number(Math.floor((Math.random() * Canvas.width) + 1), 'x');
        this.y = this.fix_number(Math.floor((Math.random() * Canvas.height) + 1), 'y')
    }

    fix_number(n: number, axis: string): number {
        let x = Math.ceil(n / this.width) * this.width;
        if (axis == 'x') x = Math.min(x, Canvas.width);
        if (axis == 'y') x = Math.min(x, Canvas.height);

        return Math.max(x, 0);

    }
}
