import { Direction } from './snake.js'

export enum Keys {
    Left = "ArrowLeft",
    Right = "ArrowRight",
    Up = "ArrowUp",
    Down = "ArrowDown",
    Space = "Space"
}

export class Controls {

    static last_key: string;

    static set_key = (ev: KeyboardEvent) => {
        Controls.last_key = ev.key;
    }

    static get_input(): Direction {

        switch (Controls.last_key) {
            //Left arrow
            case Keys.Left: {
                return Direction.Left;
            }

            //Up arrow
            case Keys.Up: {
                return Direction.Up;
            }

            //Right arrow
            case Keys.Right: {
                return Direction.Right;
            }

            //Down arrow
            case Keys.Down: {
                return Direction.Down;
            }

            //Space key
            case Keys.Space: {
                console.log('space');
            }
        }

    }
}
