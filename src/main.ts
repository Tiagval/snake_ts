import { Controls } from './controls.js'
import { Canvas } from './canvas.js'
import { Snake } from './snake.js'
import { Food } from './food.js'

export enum Speed {
    DEFAULT = 100,
    FAST = 50,
}

class Game {

    static score: number = 0;
    static is_over: boolean = false;
    static snake: Snake;
    static food: Food;
    static speed: Speed = Speed.DEFAULT;
    static game: number;

    static init() {

        let body: HTMLBodyElement = document.querySelector("body")
        body.onkeyup = Controls.set_key


        Canvas.init(<HTMLCanvasElement>document.querySelector("canvas"));

        Game.snake = new Snake(0, 0);
        Game.food = new Food();
    }

    static check_collision() {

        if (Game.snake.x == Game.food.x && Game.snake.y == Game.food.y) {
            Game.snake.feed();
            Game.score += Game.food.type;
            Game.food.respawn();
        }
    }

    static loop() {

        Canvas.clear();
        Canvas.fill_bkg();

        //Paint food
        Canvas.fill_rect(Game.food.x, Game.food.y, Game.food.width, Game.food.width, Game.food.color);
        //Update snake position using received input
        Game.snake.update(Controls.get_input());
        //Paint snake
        Canvas.fill_rect(Game.snake.x, Game.snake.y, Game.snake.size, Game.snake.size, Game.snake.color);
        for (let i of Game.snake.parts) {
            Canvas.fill_rect(i.x, i.y, Game.snake.size, Game.snake.size, Game.snake.color);
            if (Game.snake.x == i.x && Game.snake.y == i.y) {
                this.is_over = true;
            }
        }
        if (this.is_over) Game.end_game();

        Game.set_score();

        //Update in case of collision with food
        Game.check_collision();

    }

    static set_score() {
        document.getElementById("score").innerHTML = "Score: " + Game.score;

    }

    static run_game() {
        Game.init();
        Game.game = window.setInterval(Game.loop, Game.speed);
    }

    static end_game() {
        console.log('Game over');
        window.clearInterval(Game.game);
    }

}

Game.run_game();
