import { Canvas } from './canvas.js'

export enum Direction {
    Up,
    Down,
    Left,
    Right
}

type Position = {
    x: number;
    y: number;
}

function get_pos(x: number, y: number): Position {
    return {
        x: x,
        y: y,
    }
}

export class Snake {
    x: number = 0;
    y: number = 0;
    direction: Direction = Direction.Right;
    parts: Position[] = [];
    size: number = 10;
    score: number = 0;
    lives: number = 1;
    color: string = 'Magenta';

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    update(direction: Direction) {

        switch (direction) {
            case (Direction.Left): {
                if (this.direction != Direction.Right) {
                    this.direction = direction;
                }
                break;
            }
            case (Direction.Right): {
                if (this.direction != Direction.Left) {
                    this.direction = direction;
                }
                break;
            }
            case (Direction.Up): {
                if (this.direction != Direction.Down) {
                    this.direction = direction;
                }
                break;
            }
            case (Direction.Down): {
                if (this.direction != Direction.Up) {
                    this.direction = direction;
                }
                break;
            }
        }
        this.propagate_movement();
    }

    propagate_movement() {

        let last_pos = get_pos(this.x, this.y);
        if (this.parts.length > 0) this.parts[0] = last_pos;
        if (this.parts.length > 1) {
            for (let i = this.parts.length - 1; i > 0; --i) {
                this.parts[i] = this.parts[i - 1];
            }
        }

        switch (this.direction) {
            case (Direction.Left): {
                this.x -= this.size;
                if (this.x < 0) this.x = Canvas.width;
                break;
            }
            case (Direction.Right): {
                this.x += this.size;
                if (this.x > Canvas.width) this.x = 0;
                break;
            }
            case (Direction.Up): {
                this.y -= this.size;
                if (this.y < 0) this.y = Canvas.height;
                break;
            }
            case (Direction.Down): {
                this.y += this.size;
                if (this.y > Canvas.height) this.y = 0;
                break;
            }
        }
    }

    feed() {

        let pos: Position = get_pos(this.x, this.y);
        this.parts.push((pos));
    }
}
